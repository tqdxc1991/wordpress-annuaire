<?php 
/**
 *  @package QiongPlugin
 */
/*
Plugin Name: Qiong Plugin
Plugin URI:https://tqdxc1991.gitlab.io/qiong-portfolio/
Description:This is my first plugin for the evaluation of CMS
Version:1.0.0
Author:Qiong
Author URI:https://tqdxc1991.gitlab.io/qiong-portfolio/
License:none
Text Domain:qiong-plugin
 */

 defined('ABSPATH') or die('you cant access this file!');

 class QiongPlugin
 {
    function custom_post_type(){
        register_post_type('annuaire',['public'=>true,'label'=>'Annuaires']);
    }
    
      function create_post_type(){
         add_action('init',array($this,'custom_post_type'));
     }

     function register() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
	}

     function activate(){
        $this->custom_post_type();
        flush_rewrite_rules();

     }

     function deactivate(){
        flush_rewrite_rules();

    }

  


    
    function enqueue() {
		// enqueue all our scripts
		wp_enqueue_style( 'mypluginstyle', plugins_url( '/assets/mystyle.css', __FILE__ ) );
		wp_enqueue_script( 'mypluginscript', plugins_url( '/assets/myscript.js', __FILE__ ) );
	}


 }

if(class_exists('QiongPlugin')){
    $qiongPlugin = new QiongPlugin('plugin initialized! '); 
    $qiongPlugin->register();
}

 register_activation_hook(__FILE__,array($qiongPlugin,'activate'));


 register_deactivation_hook(__FILE__,array($qiongPlugin,'deactivate'));

 function shortcode_annuaires() {
    global $wpdb;
    $entreprises = $wpdb->get_results( "SELECT * FROM wp_annuaire", OBJECT );
  
        foreach($entreprises as $entreprise){
            echo "<h3>";
            echo $entreprise -> nom_entreprise;
            echo "</h3>";
            echo "<p>";
            echo $entreprise -> localisation_entreprise;
            echo " - ";
            echo $entreprise -> prenom_contact;
            echo " - ";
            echo $entreprise -> nom_contact;
            echo " - ";
            echo $entreprise -> mail_contact;
            echo "</p>";
  
        }
  }
  
  add_shortcode('annuaires', 'shortcode_annuaires');