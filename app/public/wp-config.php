<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define('WP_DEBUG',true);
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vcXtrz8hHikg+RWISX73YzM+CJP8H2sHgWl/71VVw/FAnFNAiWCl6jib0cwn/F3+Ob5C2XGwpJ5Zo2FD72bA1g==');
define('SECURE_AUTH_KEY',  'DQ2ZJAXZREXulgO2nImNLRyaidGM4mBJMn/0+Dz1ENmvCJZQzMqJ8WpgZGX/MIbTXVVIEeIa+R3s8kGnbuA15w==');
define('LOGGED_IN_KEY',    'tVZWTotAiJAN3j4CffVExM9bJJvglsZL9KftegjjWsG57uND5n6Qf/sVwyR5iLTYXU05xuf6T90SPPu0dGXbKw==');
define('NONCE_KEY',        'dtAO4En8Vz7RbTA4md4LzAuGUjy+rQ9z/Sc2VuvzxXLlone1+GqRcmR59cNFwqb5wkpjuwJafGQYsFFdcXRWTg==');
define('AUTH_SALT',        'mVok5LOdD7+EqEcFmDDS9lu3Grs0mH/Q2X2aHq6PX8tCzZ9fcBr+2H2hXY42rRHZ3DP5kSyPqzuWwxviJxR/EA==');
define('SECURE_AUTH_SALT', '6xMuGUUjvl/Ke/e5jNk1JVo3ns+9228kjACAmpNq8y9+P72BkmLkXetA4g/YnDd3V7tvlmkyXvbONaFF1limiQ==');
define('LOGGED_IN_SALT',   'q8u8hYZKqk6BJyA8ddDr1VhQtCS9AenxtZ5tFQTgQXL1oMUHhDxEpOvIvsPrziLV8XjztcNnNAm+of3jIwusHw==');
define('NONCE_SALT',       'aly0IeeIKoBSN8M9eDWwuGp8eH9GIDgSs21ZQAYY/UQs4WJNYVAq79Pu95wJW8HyVPp0Vcc7aZRO1eyk5CP8+A==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
